---
layout: job_family_page
title: "Deal Desk"
---

At GitLab, we are fundamentally changing the way our customers get their software to market by putting the entire DevOps lifecycle into a single application. With over 100,000 organizations using the product GitLab is one of the fastest growing companies in technology. Our customer success teams are responsible for ensuring that our customers are wildly successful in achieving their business outcomes with the GitLab product as they move to truly modern DevOps. A complete long-term engagement of planning, strategy, coaching, services and relationship building ensures our customers exceed their goals and digitally transform. We know that our customers trust GitLab to take their ideas all the way from plan to shipped product in production and we don’t take that responsibility lightly. We have an incredible existing, and rapidly growing, customer base, with a passionate, supportive open-source community and incredibly talented teams located in 40 countries focused on supporting them.

## Intermediate Analyst 

We're looking for an experienced Deal Desk Analyst who will be responsible for supporting the Field Teams in structuring deals, leveraging pricing strategy, quote management, facilitating contract reviews and manage overall quote-to-cash processes.  This position will work collaboratively with the Field and internal organizations such as Legal, Finance, Support and Sales Ops to ensure deals are structured and negotiated to comply with Gitlab’s policies and processes.  The candidate will need to become an expert of the Gitlab solution and help to optimize our quote-to-cash processes and tools. 
This is a great opportunity to create scalable processes to support a fast growing company where your responsibilities will increase exponentially with your ability. The role will support the vision of Gitlab’s sales leadership team and will report to our Deal Desk Manager.

### Responsibilities

* Serve as the trusted adviser to Field Team on all deal related matters, advise the team on alternative contract options, and/or value propositioning to help drive deal closures
* Manage the day-to-day quote management, pricing approvals, ensuring compliance with published sales and business approval policies
* Provide proactive support and guidance to the Field Team in order to help drive new deals as well as up-sell opportunities.
* Engage with the Field Team to accelerate deal velocity and execute on deal closure 
* Collaborate with impacted parties to identify acceptable options to facilitate deal closure
* Maintain and enforce agreed upon departmental review and approval SLAs
* Run end of period cadence to effective manage resources and provide appropriate visibility/escalations 
* Partner with Sales Operations on process/system improvements, sales enablement and special projects
* Deliver relevant training to new and existing sales people on deal desk, order processes and overall QTC best practices

### Requirements

* BA/BS degree
* 2+ years of proven success in Enterprise SaaS/B2B Industry, Deal Desk, Finance, Sales Operations or Pricing experience preferred
* Experience in Direct and Channel Sales model is highly desirable
* Experience with pricing strategy and international business preferred
* Strong understanding of software revenue recognition and order process principles required
* Highly organized, customer-focused, innovative and strong attention to details
* Excellent communicator, self-aware, transparent, collegial, and open to feedback
* Strong business acumen, strong reporting and analytics, troubleshooting, problem-solving, and project management skills
* Demonstrated ability to partner with GTM Teams and other cross functional departments
* Ability to multitask and prioritize at times of high volume
* Strong quantitative skills and highly proficient in Excel, PowerPoint, Saleforce.com, Zuora or similar CPQ tools
* Interest in GitLab, and open source software
* You share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization

## Manager (of people)

We're looking for an experienced Deal Desk Manager who will be responsible for supporting the Field Teams in structuring deals, leveraging pricing strategy, quote management, facilitating contract reviews/negotiations and manage overall quote-to-cash processes.  This position will work collaboratively with the Field and internal organizations such as Legal, Finance, Support and Sales Ops to ensure deals are structured and negotiated to comply with Gitlab’s policies and processes.  The candidate will need to become an expert of the Gitlab solution and help to optimize our quote-to-cash processes and tools. 
This is a great opportunity to create scalable processes to support a fast growing company where your responsibilities will increase exponentially with your ability. The role will support the vision of Gitlab’s sales leadership team and will report to our Director of Sales Operations. 

### Responsibilities

* Serve as the trusted adviser to Field Team on all deal related matters, advise the team on alternative contract options, and/or value propositioning to help drive deal closures
* Manage the day-to-day quote management, pricing approvals, ensuring compliance with published sales and business approval policies
* Provide proactive support and guidance to the Field Team in order to help drive new deals as well as up-sell opportunities.
* Engage with the Field Team to accelerate deal velocity and execute on deal closure 
* Collaborate with impacted parties to identify acceptable options to facilitate deal closure
* Assist to interpret contractual language and escalate to ensure deals comply with revenue recognition and operational policies
* Maintain and enforce agreed upon departmental review and approval SLAs
* Develop and drive initiatives to improve productivity, deal desk improvements, company deal policies and simplifying processes throughout the Sales Operations and Sales Organization 
* Run end of period cadence to effective manage resources and provide appropriate visibility/escalations 
* Partner with Sales Operations on process/system improvements, sales enablement and special projects
* Deliver relevant training to new and existing sales people on deal desk, order processes and overall QTC best practices

### Requirements

* BA/BS degree
* 5+ years of proven success in Enterprise SaaS/B2B Industry, Deal Desk, Finance, Sales Operations or Pricing experience preferred
* Experience in Direct and Channel Sales model is highly desirable
* Strong experience in contract/legal negotiations with enterprise customers and working with enterprise sales teams.
* Experience with pricing strategy and international business preferred
* Strong understanding of software revenue recognition and order process principles required
* Highly organized, customer-focused, innovative and strong attention to details
* Able to speak to senior audiences and command the respect of field teams
* Excellent communicator, self-aware, transparent, collegial, and open to feedback
* Strong business acumen, strong reporting and analytics, troubleshooting, problem-solving, and project management skills
* Demonstrated ability to partner with GTM Teams and other cross functional departments
* Ability to multitask and prioritize at times of high volume
* Strong quantitative skills and highly proficient in Excel, PowerPoint, Saleforce.com, Zuora or similar CPQ tools
* Interest in GitLab, and open source software
* You share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization

### Hiring Process
* Screening call with recruiter 
* Interview with the Hiring Manager
* Interview with 2-3 additional team members
* Final interview with Executive 
